# Hugh's Dotfiles

All of the files I care about in `$HOME`.


## How to install

```bash
alias config='git --git-dir=$HOME/.cfg/ --work-tree=$HOME'
echo ".cfg" >> .gitignore
git clone --bare git@gitlab.com:henxing/dotfiles.git $HOME/.cfg
config checkout
config config --local status.showUntrackedFiles no
```

The penultimate step may fail like so:

```bash
error: The following untracked working tree files would be overwritten by checkout:
    .bashrc
    .zshrc
    .gitignore
Please move or remove them before you can switch branches.
Aborting
```

Back up or remove the offending files and try again.

Next, open `.vim/vimrc` with `neovim`, i.e. `vim ~/.vim/vimrc`, this will
automatically install [vim-plug](https://github.com/junegunn/vim-plug) and all
plugins.

### If using zsh

After following the above, follow [this gist](https://gist.github.com/kevin-smets/8568070) and
configure as desired. Then run the following commands to install
[ohmyzsh](https://github.com/ohmyzsh/ohmyzsh) and plugins:

```bash
sh -c "$(wget -O- https://raw.githubusercontent.com/ohmyzsh/ohmyzsh/master/tools/install.sh)"
git clone https://github.com/zsh-users/zsh-autosuggestions ${ZSH_CUSTOM:-~/.oh-my-zsh/custom}/plugins/zsh-autosuggestions
git clone https://github.com/zsh-users/zsh-syntax-highlighting.git ${ZSH_CUSTOM:-~/.oh-my-zsh/custom}/plugins/zsh-syntax-highlighting
```
