if [ -x ~/.bashrc_bg ]; then
    source ~/.bashrc_bg
fi
# ~/.bashrc: executed by bash(1) for non-login shells.
# see /usr/share/doc/bash/examples/startup-files (in the package bash-doc)
# for examples

# If not running interactively, don't do anything
case $- in
    *i*) ;;
      *) return;;
esac

# don't put duplicate lines or lines starting with space in the history.
# See bash(1) for more options
HISTCONTROL=ignoreboth

# append to the history file, don't overwrite it
shopt -s histappend

# for setting history length see HISTSIZE and HISTFILESIZE in bash(1)
HISTSIZE=1000
HISTFILESIZE=2000

# check the window size after each command and, if necessary,
# update the values of LINES and COLUMNS.
shopt -s checkwinsize

# If set, the pattern "**" used in a pathname expansion context will
# match all files and zero or more directories and subdirectories.
shopt -s globstar

# make less more friendly for non-text input files, see lesspipe(1)
[ -x /usr/bin/lesspipe ] && eval "$(SHELL=/bin/sh lesspipe)"

# set variable identifying the chroot you work in (used in the prompt below)
if [ -z "${debian_chroot:-}" ] && [ -r /etc/debian_chroot ]; then
    debian_chroot=$(cat /etc/debian_chroot)
fi

# set a fancy prompt (non-color, unless we know we "want" color)
case "$TERM" in
    xterm-color) color_prompt=yes;;
esac

# uncomment for a colored prompt, if the terminal has the capability; turned
# off by default to not distract the user: the focus in a terminal window
# should be on the output of commands, not on the prompt
#force_color_prompt=yes

if [ -n "$force_color_prompt" ]; then
    if [ -x /usr/bin/tput ] && tput setaf 1 >&/dev/null; then
	# We have color support; assume it's compliant with Ecma-48
	# (ISO/IEC-6429). (Lack of such support is extremely rare, and such
	# a case would tend to support setf rather than setaf.)
	color_prompt=yes
    else
	color_prompt=
    fi
fi

if [ "$color_prompt" = yes ]; then
    PS1='${debian_chroot:+($debian_chroot)}\[\033[01;32m\]\u@\h\[\033[00m\]:\[\033[01;34m\]\w\[\033[00m\]\$ '
else
    PS1='${debian_chroot:+($debian_chroot)}\u@\h:\w\$ '
fi
unset color_prompt force_color_prompt

# If this is an xterm set the title to user@host:dir
case "$TERM" in
xterm*|rxvt*|screen*|tmux*)
    PS1="\[\e]0;${debian_chroot:+($debian_chroot)}\u@\h: \w\a\]$PS1"
    ;;
*)
    ;;
esac

# enable color support of ls and also add handy aliases
if [ -x /usr/bin/dircolors ]; then
    eval `dircolors $HOME/.dir_colors/dircolors`
    alias ls='ls --color=auto'
    #alias dir='dir --color=auto'
    #alias vdir='vdir --color=auto'

    alias grep='grep --color=auto'
    alias fgrep='fgrep --color=auto'
    alias egrep='egrep --color=auto'
fi

# some more ls aliases
alias ll='ls -alF'
alias delete_pyc='find . -name \*.pyc -delete'
alias tmw='tmuxinator start work'
alias clip='xclip -selection clipboard'

# Up aliases
alias up='cd ../'
alias upp='cd ../../'
alias uppp='cd ../../../'
alias upppp='cd ../../../../'
alias uppppp='cd ../../../../../'

# count lines of code in a git repo
cloc-git() {
    git clone --depth 1 "$1" temp-linecount-repo &&
    printf "('temp-linecount-repo' will be deleted automatically)\n\n\n" &&
    cloc temp-linecount-repo &&
    rm -rf temp-linecount-repo
}

if [ $(command -v nvim) ]
then
    alias vim='nvim'
fi

vim4() { vim $4 -c "split $2" -c "vsplit $1" -c 'wincmd j' -c "vsplit $3"; }

export VISUAL=nvim
export EDITOR="$VISUAL"
export PATH="$HOME/.cabal/bin:$HOME/.fzf/bin:$PATH:$HOME/.local/bin:/snap/bin"

# Git tab complete
source /usr/share/bash-completion/completions/git

# Branch in prompt
source /etc/bash_completion.d/git-prompt
GIT_PS1_SHOWDIRTYSTATE=true
export PS1='[\u@\h \W$(__git_ps1)]\$ '

# Turn off flow control
stty -ixon

# Add an "alert" alias for long running commands.  Use like so:
#   sleep 10; alert
alias alert='notify-send --urgency=low -i "$([ $? = 0 ] && echo terminal || echo error)" "$(history|tail -n1|sed -e '\''s/^\s*[0-9]\+\s*//;s/[;&|]\s*alert$//'\'')"'

# Alias definitions.
# You may want to put all your additions into a separate file like
# ~/.bash_aliases, instead of adding them here directly.
# See /usr/share/doc/bash-doc/examples in the bash-doc package.

if [ -f ~/.bash_aliases ]; then
    . ~/.bash_aliases
fi

# enable programmable completion features (you don't need to enable
# this, if it's already enabled in /etc/bash.bashrc and /etc/profile
# sources /etc/bash.bashrc).
if ! shopt -oq posix; then
  if [ -f /usr/share/bash-completion/bash_completion ]; then
    . /usr/share/bash-completion/bash_completion
  elif [ -f /etc/bash_completion ]; then
    . /etc/bash_completion
  fi
fi

[ -f ~/.fzf.bash ] && source ~/.fzf.bash
alias config='git --git-dir=$HOME/.cfg/ --work-tree=$HOME'
export DISPLAY=:0

if [ -n "${WSLENV+set}" ]; then
    export NVIM_QT_PATH='/mnt/c/Program Files/neovim-qt/bin/nvim-qt.exe'
    export DISPLAY="`grep nameserver /etc/resolv.conf | sed 's/nameserver //'`:0"
    alias settings="nvim /mnt/c/Users/henxi/AppData/Local/Packages/Microsoft.WindowsTerminal_8wekyb3d8bbwe/LocalState/settings.json"
    alias fix_clock="sudo ntpdate pool.ntp.org"
fi

MC_SKIN=$HOME/.config/mc/mc-solarized-skin/solarized.ini

export NVM_DIR="$HOME/.nvm"
[ -s "$NVM_DIR/nvm.sh" ] && \. "$NVM_DIR/nvm.sh"  # This loads nvm
[ -s "$NVM_DIR/bash_completion" ] && \. "$NVM_DIR/bash_completion"  # This loads nvm bash_completion

if command -v kubectl $> /dev/null; then
    source <(kubectl completion bash)
fi

[ -f $HOME/.cargo/env ] && . $HOME/.cargo/env
export PYTHON_KEYRING_BACKEND=keyring.backends.null.Keyring

delete_pods_by_status() {
  status=${1:-Completed}
  pods=$(kubectl get pods | awk -v status="$status" '$0 ~ status {print $1}')

  if [ -z "$pods" ]; then
    echo "No pods with status '$status' found."
  else
    echo "The following pods with status '$status' will be deleted:"
    echo "$pods"
    read -p "Are you sure you want to delete these pods? (y/N) " answer
    if [[ $answer =~ ^[Yy]$ ]]; then
      echo "Deleting pods with status '$status':"
      echo "$pods" | xargs kubectl delete pod
    else
      echo "Deletion canceled."
    fi
  fi
}

remove_poetry_virtualenv() {
    path=$(poetry env info | awk '/^Path:/ {print $2}' | head -n 1)
    if [[ "$path" == "NA" ]]
    then
        echo "No poetry environment found"
    else
        read -p "Removing poetry environment at $path, continue? [y/N] " answer
        if [[ $answer =~ ^[Yy]$ ]]; then
            poetry env remove $(poetry env info | awk '/^Python:/ {print $2}' | head -n 1)
        else
            echo "Removal canceled"
        fi
    fi
}

export GPG_TTY=$(tty)
